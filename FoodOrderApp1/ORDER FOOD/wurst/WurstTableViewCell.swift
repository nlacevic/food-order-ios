//
//  WurstTableViewCell.swift
//  FoodOrderApp1
//
//  Created by Halil on 30.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class WurstTableViewCell: UITableViewCell {
    
    @IBOutlet weak var wurstImage: UIImageView!
    
    @IBOutlet weak var wurstLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setWurst(wurst: Product) {
        setImage(from: wurst.getImage())
        wurstLabel.text = wurst.getName()
    }
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.wurstImage.image = image
            }
        }
    }
}
