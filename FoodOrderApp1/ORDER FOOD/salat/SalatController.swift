//
//  SalatController.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class SalatController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
   
    @IBOutlet weak var tableView: UITableView!
    
    static var salats: [Product] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SalatController.salats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let salat = SalatController.salats[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalatTableViewCell", for: indexPath) as! SalatTableViewCell
        cell.setSalat(salat: salat)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "descriptionSegue", sender: self)
//        print("row: \(indexPath.row)")
    }
        
    
    
    func createArray() -> [Salat] {
        var tempSalats: [Salat] = []
        let image1 = UIImage(named:"burger1") ?? UIImage()
        let salat1 = Salat(image: image1 , title: "Prvi Salat")
        let salat2 = Salat(image: image1 , title: "Drugi Salat")
        let salat3 = Salat(image: image1 , title: "Treci Salat")
        let salat4 = Salat(image: image1 , title: "Cetvrti Salat")
        let salat5 = Salat(image: image1 , title: "Peti Salat")
        
        tempSalats.append(salat1)
        tempSalats.append(salat2)
        tempSalats.append(salat3)
        tempSalats.append(salat4)
        tempSalats.append(salat5)



        return tempSalats
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //salats = createArray()
        
       setUp()
    }

    func setUp() {
        let nib = UINib(nibName: "SalatTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SalatTableViewCell")

       }


}
