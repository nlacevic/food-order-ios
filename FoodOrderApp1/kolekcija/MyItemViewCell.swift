//
//  MyItemViewCell.swift
//  FoodOrderApp1
//
//  Created by Halil on 12.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class MyItemViewCell: UICollectionViewCell {

    @IBOutlet weak var MyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp() {
        self.backgroundColor = UIColor.cyan // make cell more visible in our example project
              
    }
}
