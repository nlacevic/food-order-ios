//
//  CartController.swift
//  FoodOrderApp1
//
//  Created by Halil on 25.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class CartController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    @IBAction func backButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "burgerCartBackSegue", sender: self)
        
    }
    @IBOutlet weak var tableView: UITableView!
    
    var cartProducts = Cart.shared.getCartProducts()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartProducts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cartItem = cartProducts[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
        cell.setCart(cartItem: cartItem)
        
        return cell
    }

    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        //xib dodat
//        return "Name    Amount    Price"
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func setUp() {
        let nib = UINib(nibName: "CartTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CartTableViewCell")
       }
    
    @IBAction func checkoutButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "checkoutSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
        setTitle()
    }
    
    func setTitle() {
        tableView.register(UINib(nibName: "CartTableHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "CartTableHeader")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CartTableHeader") as! CartTableHeader
    headerView.labelTitle.text = "Name"
    headerView.amoutLabelTitle.text = "Amount"
    headerView.priceLabelTitle.text = "Price"
        
    return headerView
    }
    
}
