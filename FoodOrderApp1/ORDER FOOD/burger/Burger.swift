//
//  Burger.swift
//  FoodOrderApp1
//
//  Created by Halil on 18.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class Burger {
    
    var image: UIImage
    var title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
        
    }
}
