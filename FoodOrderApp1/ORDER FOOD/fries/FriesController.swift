//
//  FriesController.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class FriesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    static var fries: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fries = createArray()

        setUp()
    }
    
    func setUp() {
        let nib = UINib(nibName: "FriesTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "FriesTableViewCell")

       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FriesController.fries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let friess = FriesController.fries[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriesTableViewCell", for: indexPath) as! FriesTableViewCell
        cell.setFries(fries: friess)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "descriptionSegue2", sender: self)
//        print("row: \(indexPath.row)")
    }
    
    func createArray() -> [Fries] {
        var tempFries: [Fries] = []
        let image1 = UIImage(named:"burger1") ?? UIImage()
        let fries1 = Fries(image: image1 , title: "Prvi Fries")
        let fries2 = Fries(image: image1 , title: "Drugi Fries")
        let fries3 = Fries(image: image1 , title: "Treci Fries")
        let fries4 = Fries(image: image1 , title: "Cetvrti Fries")
        let fries5 = Fries(image: image1 , title: "Peti Fries")
        
        tempFries.append(fries1)
        tempFries.append(fries2)
        tempFries.append(fries3)
        tempFries.append(fries4)
        tempFries.append(fries5)



        return tempFries
    }
}
