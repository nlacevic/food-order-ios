//
//  WurstController.swift
//  FoodOrderApp1
//
//  Created by Halil on 24.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class WurstController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBAction func cartButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "cartSegue2", sender: self)
    }
    @IBOutlet weak var tableView: UITableView!
    static var wursts: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //wursts = createArray()

        setUp()
    }
    
    func setUp() {
        let nib = UINib(nibName: "WurstTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "WurstTableViewCell")

       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WurstController.wursts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let wurst = WurstController.wursts[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WurstTableViewCell", for: indexPath) as! WurstTableViewCell
        cell.setWurst(wurst: wurst)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "descriptionSegue2", sender: self)
//        print("row: \(indexPath.row)")
    }
    
    func createArray() -> [Wurst] {
        var tempWursts: [Wurst] = []
        let image1 = UIImage(named:"burger1") ?? UIImage()
        let wurst1 = Wurst(image: image1 , title: "Prvi Wurst")
        let wurst2 = Wurst(image: image1 , title: "Drugi Wurst")
        let wurst3 = Wurst(image: image1 , title: "Treci Wurst")
        let wurst4 = Wurst(image: image1 , title: "Cetvrti Wurst")
        let wurst5 = Wurst(image: image1 , title: "Peti Wurst")
        
        tempWursts.append(wurst1)
        tempWursts.append(wurst2)
        tempWursts.append(wurst3)
        tempWursts.append(wurst4)
        tempWursts.append(wurst5)



        return tempWursts
    }
    
}
