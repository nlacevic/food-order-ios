//
//  SalatTableViewCell.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class SalatTableViewCell: UITableViewCell {

    @IBOutlet weak var salatImage: UIImageView!
    @IBOutlet weak var salatLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSalat(salat: Product) {
        setImage(from: salat.getImage())
        salatLabel.text = salat.getName()
    }
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.salatImage.image = image
            }
        }
    }}
