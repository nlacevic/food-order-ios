//
//  AboutViewController.swift
//  FoodOrderApp1
//
//  Created by Halil on 28.08.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBAction func backButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "AboutBackSegue", sender: self)
    }

    @IBAction func swipeBack(_ sender: Any) {
        self.performSegue(withIdentifier: "AboutBackSegue", sender: self)
        
    }
    
    
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let po = ProductOption("Nesto", 12)
        let options = [po,po]

        let ps = ProductSpecificator(ProductSpecificator.SpecificatorType.RADIO_BUTTON, options)
        let extras = [("Velicina",ps)]
        let p = Product(1,"Ime",5.5,"OPIS",extras)
       
        productName.text = p.getExtras()[0].0
//        productDescription.text = p.getDescription()
//        productPrice.text = String(format:"%.2f", p.getPrice())
    }
//    func swipeToPop() {
//
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
//    }


}
