//
//  customerDetails.swift
//  FoodOrderApp1
//
//  Created by Halil on 27.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class CustomerDetails: UIViewController{
    
    @IBAction func backButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "backDetailsSegue", sender: self)
    }
    
    
    @IBAction func payButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "payPaypalSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
