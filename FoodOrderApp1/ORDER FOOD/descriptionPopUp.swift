//
//  descriptionPopUp.swift
//  FoodOrderApp1
//
//  Created by Halil on 25.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit


class descriptionPopUp: UIViewController {
    
    @IBOutlet weak var descriptionPrice: UILabel!
    @IBOutlet weak var descriptionImage: UIImageView!

    @IBOutlet weak var descriptionName: UILabel!
    
    @IBOutlet weak var descriptionAbout: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBAction func stepperValue(_ sender: UIStepper) {
        self.amountLabel.text = Int(sender.value).description
            descriptionPrice.text = String((sender.value) * descriptionPopUp.productDescribed.getPrice())
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "popUpBackSegue", sender: self)
    }
    
    @IBAction func addToCartPressed(_ sender: Any) {
        let count = (amountLabel.text! as NSString).integerValue
        let itemToAdd = ProductWrapper(descriptionPopUp.productDescribed, count)
        Cart.shared.addProduct(itemToAdd)
        let alert = UIAlertController(title: "", message: "Item added to cart!", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
          // your code with delay
            alert.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: "popUpBackSegue", sender: nil)
            })
        }
    }
    static var productDescribed = Product(-1, "Error", 0, "Error")
    func setAmountLabel(){
        amountLabel.text = Int(1).description
    }
    
 
    func setUp() {
        descriptionPrice.text = String(descriptionPopUp.productDescribed.getPrice())
        descriptionName.text = descriptionPopUp.productDescribed.getName()
        descriptionAbout.text = descriptionPopUp.productDescribed.getDescription()
        setImage(from: descriptionPopUp.productDescribed.getImage())
        descriptionImage.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        descriptionImage.contentMode = .scaleAspectFill
        view.addSubview(descriptionImage)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAmountLabel()
        setUp()
    }
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.descriptionImage.image = image
            }
        }
    }}

