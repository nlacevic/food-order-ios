//
//  DemoHeader.swift
//  FoodOrderApp1
//
//  Created by Halil on 18.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class CartTableHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var labelTitle: UILabel!
 
    @IBOutlet weak var amoutLabelTitle: UILabel!
    
    @IBOutlet weak var priceLabelTitle: UILabel!
    
}
