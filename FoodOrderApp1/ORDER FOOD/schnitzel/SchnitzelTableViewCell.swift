//
//  SchnitzelTableViewCell.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class SchnitzelTableViewCell: UITableViewCell {

    @IBOutlet weak var schnitzelImage: UIImageView!
    @IBOutlet weak var schnitzelLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSchnitzel(schnitzel: Product) {
        setImage(from: schnitzel.getImage())
        schnitzelLabel.text = schnitzel.getName()
    }
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.schnitzelImage.image = image
            }
        }
    }
}
