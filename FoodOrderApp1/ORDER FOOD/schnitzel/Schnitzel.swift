//
//  Schnitzel.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class Schnitzel {
    
    var image: UIImage
    var title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
        
    }
}
