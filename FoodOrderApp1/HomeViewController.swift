//
//  ViewController.swift
//  FoodOrderApp1
//
//  Created by Halil on 23.08.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit



class HomeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    
    @IBAction func aboutButtonPressed(_ sender: Any) {
        print("Button pressed")
        self.performSegue(withIdentifier: "AboutViewSegue", sender: self)
    }
    
    @IBAction func couponCodeuttonPressed(_ sender: Any) {
        print("Button pressed")
        self.performSegue(withIdentifier: "CouponCodeSegue", sender: self)
    }
    
    @IBAction func orderFoodButtonPressed(_ sender: Any) {
        print("Button pressed")
        self.performSegue(withIdentifier: "OrderFoodSegue", sender: self)
    }
    
    
    @IBAction func locationButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "LocationSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View has loaded")
        getRequest()
        let probniProduct = Product(1, "Proba", 2.5, "Neki descr")
        let pw1 = ProductWrapper(probniProduct, 1)
        var products = [ProductWrapper]()
        products.append(pw1)
        let probniProduct1 = Product(2, "Proba2", 4.5, "Neki descr")
        let pw2 = ProductWrapper(probniProduct1, 4)
        products.append(pw2)
        
        Cart.shared.setCartProducts(products)
    }
    
    func getRequest() {
        guard let url = URL(string: "http://www.cloudfood01.printgrafix.de/admin/api/jsonForProducts.php") else { return }
               
               let session = URLSession.shared
               session.dataTask(with: url) { (data, response, error) in
                   if let response = response {
                       print(response)
                   }
                   
                   if let data = data {
                       print(data)
                       do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                           print(json)
                        
                        var hamburgeri = [Product]()
                        var salat = [Product]()
                        var wurst = [Product]()
                        var schnitzel = [Product]()
                        var frites = [Product]()
                        
                        let jsonArray = json as! [AnyObject]
                                for jsonObject in jsonArray {
                                    //Define product by product from JSON
                                    var productName = ""
                                    if let name = jsonObject["name"] as? String{
                                        productName = name
                                    }
                                    else {
                                        productName = "Error"
                                    }
                                    var productDescription = ""
                                    if let description = jsonObject["description"] as? String{
                                        productDescription = description
                                    }
                                    else {
                                        productDescription = "Error"
                                    }
                                    var productId = 0
                                    if let id = (jsonObject["id"] as? NSString)?.integerValue {
                                        productId = id
                                    }
                                    else {
                                        continue
                                    }
                                    var productImageUrl = ""
                                    if let imageUrl = jsonObject["image_URL"] as? String {
                                        productImageUrl = imageUrl
                                    }
                                    else{
                                        productImageUrl = "" //DODATI DEFAULT SLIKU
                                    }
                                    var productPrice = 0.0
                                    if let price = (jsonObject["price"] as? NSString)?.doubleValue {
                                        productPrice = price
                                    }
                                    else{
                                        productPrice = 0.0
                                    }
                                    var productType = ""
                                    if let type = jsonObject["type"] as? String{
                                        productType = type
                                    }
                                    else{
                                        productType = "" //DODATI DEFAULT TYPE
                                    }
                                    var jsonString = (jsonObject["extras"] as! String)
                                    jsonString.append("]")
                                    jsonString.insert("[", at: jsonString.startIndex)
                                    let extrasData = jsonString.data(using: .utf8)!
                                    let extrasJSON = try JSONSerialization.jsonObject(with: extrasData, options: [])
                                    
                                    
                                    var productExtras = [(String, ProductSpecificator)]()
                                    
                                    if let ext = extrasJSON as? [AnyObject] {
                                        for obj in ext{
                                            let jsonExtras = obj["extras"] as! [AnyObject]
                                        for object in jsonExtras{
                                            
                                            var poList = [ProductOption]()
                                            var extrasName = ""
                                            if let name = object["name"] as? String{
                                                extrasName = name
                                            }
                                            else{
                                                extrasName = "Error"
                                            }
                                            if let ps = object["ps"] as? [String : Any]{
                                                var psType = ""
                                                
                                                if let type = ps["type"] as? String{
                                                    psType = type
                                                }
                                                
                                                else{
                                                    psType = "" //Default
                                                }
                                                if let options = ps["options"] as? [[String : Any]]{
                                                    for op in options{
                                                        var poName = ""
                                                        if let name = op["name"] as? String{
                                                            poName = name
                                                        }
                                                        else{
                                                            poName = "" //Default
                                                        }
                                                        var poPrice = 0.0
                                                        if let price = (op["price"] as? NSString)?.doubleValue {
                                                            poPrice = price
                                                        }
                                                        let po = ProductOption(poName, poPrice)
                                                        poList.append(po)
                                                    }
                                                    var specificator = ProductSpecificator(ProductSpecificator.SpecificatorType.RADIO_BUTTON)
                                                    switch psType{
                                                    case "R":
                                                        specificator = ProductSpecificator(ProductSpecificator.SpecificatorType.RADIO_BUTTON, poList)
                                                    case "C":
                                                        specificator = ProductSpecificator(ProductSpecificator.SpecificatorType.CHECKBOX, poList)
                                                    default:
                                                        print("Specificator Type error (Ja dodao)")
                                                    }
                                                    
                                                    productExtras.append((extrasName, specificator))
                                                    
                                                }
                                                else{
                                                    continue
                                                }
                                            }
                                            else{
                                                continue
                                            }
                                            
                                        }
                                        }
                                    }
                                
                                   
                                    
                                    let product = Product(productId, productName, productPrice, productDescription, productImageUrl, productExtras)
                                    
                                    switch productType {
                                    case "hamburger":
                                        hamburgeri.append(product)
                                    
                                    case "salat":
                                        salat.append(product)
                                    
                                    case "wurst":
                                        wurst.append(product)
                                        
                                    case "schnitzel":
                                        schnitzel.append(product)
                                        
                                    case "frites":
                                        frites.append(product)
                                        
                                    default:
                                        print("Nista")
                                    }
                                    
                                }
                        BurgerController.burgers = hamburgeri
                        WurstController.wursts = wurst
                        SchnitzelController.schnitzels = schnitzel
                        SalatController.salats = salat
                        FriesController.fries = frites
                       } catch {
                           print(error)
                       }
                       
                   }
               }.resume()
    }
    
}


