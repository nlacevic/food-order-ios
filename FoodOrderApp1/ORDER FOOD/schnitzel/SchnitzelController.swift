//
//  SchnitzelController.swift
//  FoodOrderApp1
//
//  Created by Halil on 07.10.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class SchnitzelController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    static var schnitzels: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //schnitzels = createArray()

        setUp()
    }
    
    func setUp() {
        let nib = UINib(nibName: "SchnitzelTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SchnitzelTableViewCell")

       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SchnitzelController.schnitzels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schnitzel = SchnitzelController.schnitzels[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchnitzelTableViewCell", for: indexPath) as! SchnitzelTableViewCell
        cell.setSchnitzel(schnitzel: schnitzel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "descriptionSegue", sender: self)
//        print("row: \(indexPath.row)")
    }
        
    
    
    func createArray() -> [Schnitzel] {
        var tempSchnitzels: [Schnitzel] = []
        let image1 = UIImage(named:"burger1") ?? UIImage()
        let schnitzel1 = Schnitzel(image: image1 , title: "Prvi schnitzel")
        let schnitzel2 = Schnitzel(image: image1 , title: "Drugi schnitzel")
        let schnitzel3 = Schnitzel(image: image1 , title: "Treci schnitzel")
        let schnitzel4 = Schnitzel(image: image1 , title: "Cetvrti schnitzel")
        let schnitzel5 = Schnitzel(image: image1 , title: "Peti schnitzel")
        
        tempSchnitzels.append(schnitzel1)
        tempSchnitzels.append(schnitzel2)
        tempSchnitzels.append(schnitzel3)
        tempSchnitzels.append(schnitzel4)
        tempSchnitzels.append(schnitzel5)



        return tempSchnitzels
    }

}
