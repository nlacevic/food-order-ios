//
//  ProductSpecificator.swift
//  FoodOrderApp1
//
//  Created by Halil on 01.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

public class ProductSpecificator {
        
    public enum SpecificatorType {
        case RADIO_BUTTON
        case CHECKBOX
    }
    private var specificatorType: SpecificatorType
    private var options = [ProductOption]()
    
    public init(_ specificatorType: SpecificatorType, _ options: [ProductOption]) {
        self.specificatorType = specificatorType
        self.options = options
    }
    
    public init(_ specificatorType: SpecificatorType){
        self.specificatorType = specificatorType
        self.options = []
        
    }
    
    public func getSpecificatorType() -> SpecificatorType {
        return specificatorType
    }


    public func setSpecificatorType(_ specificatorType: SpecificatorType) -> Void{
        self.specificatorType = specificatorType
    }

    public func getOptions() -> [ProductOption]{
        return options
    }

    public func setOptions(_ options: [ProductOption]) -> Void{
        self.options = options
    }
    
    public func addOption(_ option: ProductOption) -> Void {
        self.options.append(option)
    }

    public func removeOption(_ option: ProductOption) -> Void{
       self.options.removeAll { (po) -> Bool in
                    po.getName().elementsEqual(option.getName())
                }
                    
          
        }
    }

    
//
//       public void removeOption(ProductOption option) {
//           for(ProductOption po : this.options) {
//               if (po.getName().equals(option.getName())) {
//                   this.options.remove(po);
//                   break;
//               }
//
    
    

