//
//  BurgerController.swift
//  FoodOrderApp1
//
//  Created by Halil on 15.09.20.
//

import UIKit

class BurgerController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBAction func cartButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "cartSegue", sender: self)
    }
    
    @IBAction func backBurgerPressed(_ sender: Any) {
        performSegue(withIdentifier: "burgerBackSegue", sender: self)
    }
    
    @IBOutlet weak var tableView: UITableView!
    static var burgers: [Product] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BurgerController.burgers.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let burger = BurgerController.burgers[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BurgerTableViewCell", for: indexPath) as! BurgerTableViewCell
        cell.setBurger(burger: burger)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        descriptionPopUp.productDescribed = BurgerController.burgers[indexPath.row]
        performSegue(withIdentifier: "descriptionSegue", sender: BurgerController.burgers[indexPath.row])
//        print("row: \(indexPath.row)")
    }
    

    override func viewDidLoad() {
       super.viewDidLoad()        
       setUp()
    }

    func setUp() {
        let nib = UINib(nibName: "BurgerTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "BurgerTableViewCell")

       }


}
