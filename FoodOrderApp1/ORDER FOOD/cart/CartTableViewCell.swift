//
//  CartTableViewCell.swift
//  FoodOrderApp1
//
//  Created by Halil on 25.09.20.
//  Copyright © 2020 Halil. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    var cartItemGlobal = ProductWrapper(Product(-1,"Error",0,"Error"),0)
    
    @IBAction func deleteProduct(_ sender: Any) {
        Cart.shared.removeProduct(cartItemGlobal)
        Cart.shared.calculateTotalPrice()
        
    }
    @IBOutlet weak var amountValue: UIStepper!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productNotes: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBAction func stepperValue(_ sender: UIStepper) {
        self.amountLabel.text = Int(sender.value).description
        try?
        cartItemGlobal.setCount(Int(sender.value))
        cartItemGlobal.calculateTotalPrice()
        productPrice.text = String( cartItemGlobal.getTotalPrice())
        
    }
    
    
    
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setCart(cartItem: ProductWrapper) {
        amountLabel.text = String(cartItem.getCount())
        productName.text = cartItem.getProduct().getName()
        productPrice.text = String( cartItem.getTotalPrice())
        amountValue.value = Double(cartItem.getCount())
        cartItemGlobal = cartItem
    }
    
    //    override func layoutSubviews() {
//        super.layoutSubviews()
//        setAmountLabel()
//    }

    func setAmountLabel(){
        amountLabel.text = Int(1).description
    }
    
   
        
}
